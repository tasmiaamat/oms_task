@extends('welcome')

@section('content')

<div class="container-fluid">
   <!-- Basic Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Basic Table</strong> <small>Basic all information about tasks</small> </h2>
                        <div class="text-right">
                        <a href="{{route('task_manager_route')}}"><button class="btn btn-primary">Create Task</button></a>
                    </div>

                    </div>
                    <div class="body table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Employee Name</th>
                                    <th>Month</th>
                                    <th>Task Name</th>
                                    <th>Task Description</th>
                                    <th>Complexity</th>
                                    <th>Task Type</th>
                                    <th>Task Supervisor</th>
                                    <th>Allocation Time</th>
                                    <th>Allocated Time</th>
                                    <th>Task Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            	@foreach($employees as $key =>$employee)
                                @php
                               
                                /*$a=strtotime($employee->allocation_time);
                                $dbDate=date('Y-m-d', $a);
                                $dbTime=date('G.i', $a);
                                $time=$dbTime - (strtotime($employee->allocation_time)* 60);
                               // $current =date('G.i', $time);
                                dd($time);
                                $today=date('Y-m-d');
                                if ($dbDate <= $today ) {
                                  //  $time = date('Gi.s', $timestamp);
                                   echo 'time';
                                } else
                                {
                                    echo 'false';
                                }*/

                                $to_time = strtotime(date('Y-m-d H:i:s'));
                                $from_time = strtotime($employee->allocation_time);
                                $time= round(abs($to_time - $from_time) / 3600,2);
                                //dd($time -$employee->allocated_time);
                                @endphp
                            
                                <tr  class="@php 
                                        if (($time > $employee->allocated_time) && $employee->task_status == '0' ) {
                                            echo 'timeover';# code...
                                        }
                                    @endphp">
                                    <td scope="row">{{++$key}}</td>
                                    <td scope="row">
                                        {{ $employee->employee->first_name}}
                                        {{ $employee->employee->last_name}}
                                    </td>
                                    <td scope="row">@php echo date('F Y', strtotime($employee->month))@endphp</td>
                                    <td scope="row">{{$employee->task_name}}</td>
                                    <td scope="row">{{$employee->task_description}}</td>
                                    <td scope="row">{{$employee->complexity ? 'easy' : 'hard'}}</td>
                                    <td scope="row">{{$employee->type ? 'team' : 'individual'}}</td>
                                    <td scope="row">
                                        {{ $employee->supervisor->first_name}}.
                                        {{ $employee->supervisor->last_name}}
                                    </td>
                                    <td scope="row">{{ date('M j,Y h:i a', strtotime($employee->allocation_time)) }} </td>
                                    <td scope="row">{{$employee->allocated_time}} hours</td>
                                    <td scope="row" class="status"> <span class="badge  {{$employee->task_status == '0' ? 'badge-info' : 'badge-success'}}"> {{$employee->task_status == '0' ? 'on going' : 'completed'}}</span></td>
                                    <td width="130px">
                                        <a href="{{route('view_tasks_route',$employee->id)}}"><button class="btn btn-theme btn-icon transparent hidden-sm-down m-l-10" type="button">
                                            <i class="zmdi zmdi-eye"></i>
                                        </button></a>

                                        <a href="{{route('edit_task_manager_route',$employee->id)}}"><button class="btn transparent btn-theme btn-icon  hidden-sm-down m-l-10" type="button">
                                            <i class="zmdi zmdi-edit"></i>
                                        </button></a>

                                          <button class="btn transparent btn-theme btn-icon  hidden-sm-down m-l-10" type="button">
                                            <i class="zmdi zmdi-fire"></i>
                                        </button>

                                        <button class="btn transparent btn-theme btn-icon  hidden-sm-down m-l-10" type="button">
                                            <i class="zmdi zmdi-block"></i>
                                        </button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Table --> 

</div>

@endsection

