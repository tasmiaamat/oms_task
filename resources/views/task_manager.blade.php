@extends('welcome')
@section('content')
<div class="container-fluid">
  <div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12" style="padding-top: 15px;">
      <div class="card">
        <div class="header">
          <h2><strong>Task Management</strong> </h2> 
        </div>
        <form method="POST" action="{{ route('save_task_manager_route') }}">
        @csrf

        <!-- cross site request foreignkey -->
        <div class="body">
          <div class="row clearfix">
            <div class="col-sm-3">
              <label>Employee Name</label>
                <select name="employee_id" class="form-control employee_id select2">
                  <option value=" ">Select Employee</option>
                  @foreach($employees as $employee)
                    <option value="{{$employee->id}}">{{$employee->name}}</option>
                  @endforeach
                </select>
            </div>

            <div class="col-sm-3">
                <label>Designation</label>
                  <select name="designation_id" class="form-control designation_id ">
                    <option value=" ">Select Designation</option>
                      @foreach($designations as $designation)
                        <option value="{{$designation->id}}">{{$designation->name}}</option>
                      @endforeach
                  </select>
            </div>

            <div class="col-sm-3">
                <label>Line Supervisor</label>
                <select name="supervisor_id" class="form-control supervisor_id " >
                  <option value=" ">Select Line Supervisor</option>
                    @foreach($employees as $employee)
                      <option value="{{$employee->id}}">{{$employee->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-sm-3">
              <div class="form-group">
                <label>Month</label>
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="zmdi zmdi-calendar"></i>
                    </span>
                      <input id="month" type="text" class="form-control" name="month" value="{{date("Y-m")}}">
                  </div>
              </div>
            </div>
          </div>

          <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
              <div class="card">
                <div class="header">
                  <h2><strong>Task Information</strong> </h2>
                </div>
                <div class="body table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>Task ID</th>
                        <th>Task Name</th>
                        <th>Task Description</th>
                        <th>Complexity</th>
                        <th>Task Type</th>
                        <th>Task Supervisor</th>
                        <th>Allocation Time</th>
                        <th>Allocated Time</th>
                      </tr>
                    </thead>
                    <tbody>
                          <tr>
                            <td tabindex="1">
                              <input type="text" class="form-control select2{{ $errors->has('id') ? ' is-invalid' : '' }}"  name="id" placeholder="Task ID" style=" width: 116px; " required>
                            </td>

                            <td tabindex="2">
                              <input type="text" class="form-control select2{{ $errors->has('task_name') ? ' is-invalid' : '' }}" name="task_name" placeholder="Task Name" style="
                            width: 116px;" required>
                            </td>

                            <td tabindex="3">
                              <input type="text" class="form-control select2{{ $errors->has('task_description') ? ' is-invalid' : '' }}" name="task_description" placeholder="Task Description" style=" width: 116px;" required>
                            </td>

                            <td tabindex="4">
                              <select class="form-control select2{{ $errors->has('complexity') ? ' is-invalid' : '' }}" name="complexity" required>
                                <option value="">Select</option>
                                <option value="0">Easy</option>
                                <option value="1">Hard</option>
                              </select>
                            </td>

                            <td tabindex="5">
                              <select class="form-control select2{{ $errors->has('type') ? ' is-invalid' : '' }}" name="type" required>
                                <option value="">Select</option>
                                <option value="0">Team</option>
                                <option value="1">Individual</option>
                              </select>
                            </td>

                            <td tabindex="6">
                              <select class="form-control select2" name="task_supervisor" required>
                                <option value=" ">Select Employee</option>
                                @foreach($employees as $employee)
                                <option value="{{$employee->id}}">{{$employee->name}}</option>
                                @endforeach
                              </select>
                            </td>

                            <td tabindex="7">
                              <div class="input-group">
                                <span class="input-group-addon">
                                <i class="zmdi zmdi-calendar"></i>
                              </span>
                              <input type="text" class="form-control select2 dateTime{{ $errors->has('allocation_time') ? ' is-invalid' : '' }} " name="allocation_time" required>
                            </div>
                            </td>
                            <td tabindex="8">
                              <div class="input-group">
                                <span class="input-group-addon">
                                  <i class="zmdi zmdi-calendar"></i>
                                </span>
                                  <input type="number" class="form-control select2{{ $errors->has('allocated_time') ? ' is-invalid' : '' }}  " name="allocated_time" required>
                              </div>
                            </td>
                          </tr>
                    </tbody>
                  </table>
                    <div class="row m-t-20 m-b-20 text-center">
                        <div class="col-12">
                          <div style="display: inline-flex;">
                            <button type="reset" value="reset" class="btn btn-raised btn-danger btn-round waves-effect">Reset</button>
                          </div>

                          <div style="display: inline-flex;">
                            <button type="submit" class="btn btn-raised btn-success btn-round waves-effect saveBtn">Submit</button>   
                          </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>
@endsection
@section('custom_js')
<script>
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(function() {

  $('.employee_id').change(function(){
    $this = $(this),
    $.ajax({
      type: "POST",
      url: "{{ route('get_employee_info_route') }}",
      data: {employee_id : $this.val() },
      cache: false,
      success: function(result){
      $('.designation_id').val(result.designation_id).select2({
  disabled: true
});
      $('.supervisor_id').val(result.supervisor_id).select2({
  disabled: true
});
      }
      });
  });
});

</script>
@endsection