<!doctype html>
<html class="no-js " lang="en">
    <head>
        <meta charset="utf-8">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Laravel') }}</title>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
        <title>:: Compass Bootstrap4 Admin ::</title>
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <!-- Favicon-->
        <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/plugins/morrisjs/morris.css')}}"/>
        <link href="{{asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" />
        <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}}"/>
        <!-- Custom Css -->
        <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
        <link rel="stylesheet" href="{{asset('assets/plugins/select2/dist/css/select2.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/hm-style.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/color_skins.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">
        
    </head>
    <body class="theme-cyan index2">
        <!-- Page Loader -->
        <!-- <div class="page-loader-wrapper">
            <div class="loader">
                <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Compass"></div>
                <p>Please wait...</p>
            </div>
        </div> -->
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
        <!-- Top Bar -->
        <nav class="navbar">
            <div class="col-12">
                <div class="navbar-header">
                    <a href="javascript:void(0);" class="h-bars"></a>
                    <a class="navbar-brand" href="index.html"><img src="assets/images/logo.svg" width="30" alt="Compass"><span class="m-l-10">Compass</span></a>
                </div>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"><i class="zmdi zmdi-notifications"></i>
                        <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right slideDown">
                        <li class="header">NOTIFICATIONS</li>
                        <li class="body">
                            <ul class="menu list-unstyled">
                                <li> <a href="javascript:void(0);">
                                    <div class="icon-circle bg-blue"><i class="zmdi zmdi-account"></i></div>
                                    <div class="menu-info">
                                        <h4>8 New Members joined</h4>
                                        <p><i class="zmdi zmdi-time"></i> 14 mins ago </p>
                                    </div>
                                </a> </li>
                                <li> <a href="javascript:void(0);">
                                    <div class="icon-circle bg-amber"><i class="zmdi zmdi-shopping-cart"></i></div>
                                    <div class="menu-info">
                                        <h4>4 Sales made</h4>
                                        <p> <i class="zmdi zmdi-time"></i> 22 mins ago </p>
                                    </div>
                                </a>
                            </li>
                            
                        </ul>
                    </li>
                    <li class="footer"> <a href="javascript:void(0);">View All Notifications</a> </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" class="fullscreen hidden-sm-down" data-provide="fullscreen" data-close="true"><i class="zmdi zmdi-fullscreen"></i></a>
            </li>
            <li><a href="sign-in.html" class="mega-menu" data-close="true"><i class="zmdi zmdi-power"></i></a></li>
            <li class=""><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="zmdi zmdi-settings"></i></a></li>
        </ul>
    </div>
</nav>
<div class="menu-container">
    <div class="menu">
        <ul>
            <li><a href="{{route('dashboard_route')}}"><i class="zmdi zmdi-home"></i>Dashboard</a></li>
            
            <li><a href="{{route('task_manager_Lists_route')}}"><i class="zmdi zmdi-home"></i>Management Lists</a></li>
        </li>
    </ul>
</div>
</div>
<!-- Right Sidebar -->
<aside id="rightsidebar" class="right-sidebar">
<ul class="nav nav-tabs">
    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#setting"><i class="zmdi zmdi-settings zmdi-hc-spin"></i></a></li>
    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#chat"><i class="zmdi zmdi-comments"></i></a></li>
    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#activity">Activity</a></li>
</ul>
<div class="tab-content">
    <div class="tab-pane active slideRight" id="setting">
        <div class="slim_scroll">
            <div class="card">
                <h6>Skins</h6>
                <ul class="choose-skin list-unstyled">
                    <li data-theme="purple">
                        <div class="purple"></div>
                    </li>
                    <li data-theme="blue">
                        <div class="blue"></div>
                    </li>
                    <li data-theme="cyan" class="active">
                        <div class="cyan"></div>
                    </li>
                    <li data-theme="green">
                        <div class="green"></div>
                    </li>
                    <li data-theme="orange">
                        <div class="orange"></div>
                    </li>
                    <li data-theme="blush">
                        <div class="blush"></div>
                    </li>
                </ul>
            </div>
            <div class="card">
                <h6>Left Menu</h6>
                <ul class="list-unstyled theme-light-dark">
                    <li>
                        <div class="t-light btn btn-default btn-simple btn-round">Light</div>
                    </li>
                    <li>
                        <div class="t-dark btn btn-default btn-round">Dark</div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
</aside>
<section class="content">
{{-- <div class="block-header">
    <div class="row">
        <div class="col-lg-7 col-md-6 col-sm-12">
            <h2>Horizontal Menu Dashboad
            <small class="text-muted">Welcome to Compass</small>
            </h2>
        </div>
    </div>
</div> --}}
@yield('content')

</section>
<!-- Jquery Core Js -->
<script src="{{asset('assets/bundles/libscripts.bundle.js')}}"></script> <!-- Lib Scripts Plugin Js -->
<script src="{{asset('assets/bundles/vendorscripts.bundle.js')}}"></script> <!-- Lib Scripts Plugin Js -->
<script type="text/javascript" src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/plugins/momentjs/moment.js')}}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/select2/dist/js/select2.min.js')}}"></script>
@yield('custom_js')
<script>
/*global $ */
$(document).ready(function() {
$(".select2").select2();
$('.date').bootstrapMaterialDatePicker({ weekStart : 0, time: false });
//$('.dateTime').bootstrapMaterialDatePicker({ weekStart : 0, time: true, format: 'YYYY-MM-DD hh:mm:ss A' });
$(function () {
        $('.dateTime').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD H:m:s',
            minDate : new Date(),
            clearButton: true,
            weekStart: 1,
            time: true,
            date: true
        });
    });
"use strict";
$('.menu > ul > li:has( > ul)').addClass('menu-dropdown-icon');
//Checks if li has sub (ul) and adds class for toggle icon - just an UI

$('.menu > ul > li > ul:not(:has(ul))').addClass('normal-sub');

$(".menu > ul > li").hover(function(e) {
if ($(window).width() > 943) {
$(this).children("ul").stop(true, false).fadeToggle(150);
e.preventDefault();
}
});
//If width is more than 943px dropdowns are displayed on hover
$(".menu > ul > li").click(function() {
if ($(window).width() <= 943) {
$(this).children("ul").fadeToggle(150);
}
});
//If width is less or equal to 943px dropdowns are displayed on click (thanks Aman Jain from stackoverflow)

$(".h-bars").click(function(e) {
$(".menu > ul").toggleClass('show-on-mobile');
e.preventDefault();
});
//when clicked on mobile-menu, normal menu is shown as a list, classic rwd menu story (thanks mwl from stackoverflow)

});
</script>

</body>
</html>