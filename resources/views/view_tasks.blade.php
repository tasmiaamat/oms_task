@extends('welcome')
@section('content')
<div class="block-header">
    <div class="row">
        <div class="col-lg-7 col-md-6 col-sm-12">
            <h2> View
            <small class="text-muted"></small>
            </h2>
        </div>
        <div class="col-lg-5 col-md-6 col-sm-12">
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-6">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-lg-6 ">
                            <h2>
                                <strong> Employee : </strong> {{ $viewList->employee->first_name }}  {{ $viewList->employee->last_name }}
                            </h2>   
                            <hr>
                            <h6 class="media-txt">
                            <small>
                                <strong>Designation : </strong>{{ $viewList->employee->designation->name }}
                            </small>
                            </h6>
                            <h6 class="media-txt">
                            <small>
                                <strong>Line Supervisor : </strong>{{$viewList->employee->supervisor->first_name}} {{$viewList->employee->supervisor->last_name}}
                            </small>
                            </h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-lg-6 ">
                            <h2>
                                <strong> Task :</strong> New Task
                             </h2>
                            <hr>
                            <h6 class="media-txt">
                            <small>
                                <strong>Task Type : </strong>{{$viewList->type ? 'team' : 'individual'}}
                            </small>
                            </h6>
                            <h6 class="media-txt">
                            <small>
                                <strong>Task Supervisor : </strong>{{$viewList->supervisor->first_name}} {{$viewList->supervisor->last_name}}
                            </small>
                            </h6>
                            <h6 class="media-txt">
                            <small>
                                <strong>Allocation Time : </strong>{{ date('M j,Y h:i a', strtotime($viewList->allocation_time)) }} 
                            </small>
                            </h6>
                            <h6 class="media-txt">
                            <small>
                                <strong>Allocated Time : </strong>{{$viewList->allocated_time}}
                            </small>
                            </h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <strong> Task Description </strong> 
                    </h2>
                    <hr>
                    <h6 class="media-txt">
                    <small>{{$viewList->task_description}}</small>
                    </h6>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
