<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Employees;
use App\TaskManager;
use App\Designation;
use App\Users;

class TaskManagement extends Controller
{

	public function dashboard()
	{
		return view('dashboard');
	}

	public function taskManager()
	{
	 	//$employees=TaskManager::with('employee')->get();
		$supervisor=Employees::join('users', 'users.id', '=', 'employees.user_id')
			->select('users.id')
			->selectRaw('CONCAT(employees.first_name," ", employees.last_name) as name')
			->where('users.status', '1')
			->get();
		//dd($supervisor->toArray());
		//$employee=TaskManager::with('employee')->get();
		//dd($employee->toArray());
		$designations=Designation::get();
		//dd($designations->toArray());
		return view('task_manager',['employees'=>$supervisor,'designations'=>$designations]);
	}
	//where('employee_id', $request->employee_id)->update(array('status' => 'inactive','to_date'=> date('Y-m-d')));

	public function saveTaskManager(Request $request)
	{
		//dd($request->all());
		/*$validatedData = $request->validate([
	        'employee_id' => 'required|unique:posts|max:255',
	        'task_name' => 'required',
	    ]);*///  dd(strtotime$request->allocation_time);
		$createTask=new TaskManager();
		$createTask->employee_id=$request->employee_id;
		$createTask->month=$request->month;
		$createTask->task_name=$request->task_name;
		$createTask->task_description=$request->task_description;
		$createTask->complexity=$request->complexity;
		$createTask->type=$request->type;
		$createTask->task_supervisor=$request->task_supervisor;
		$createTask->allocation_time=$request->allocation_time;
		//$createTask->allocation_date=$request->allocation_date;
		$createTask->allocated_time=$request->allocated_time;
		//$createTask->task_status=$request->task_status;
		//dd($createTask->toArray());
		//dd($createTask->toArray());
		$createTask->save();
		return redirect()->route('task_manager_Lists_route');
	}

	public function taskManagerLists()
	{
		//$employee=Employees::get();
		$employee=TaskManager::with(['employee','supervisor'])->get();
		//dd($employee->toArray());
		//dd($info->toArray());
		return view('task_manager_Lists',['employees'=>$employee]);
	}
	//ajax for dropdown value
	public function getEmployeeInfo(Request $request)
	{

		$employee_id=$request->employee_id;

		$employee=Employees::where('user_id',$employee_id)->first();
		return $empData =[
			'designation_id' => $employee->designation_id ,
			'supervisor_id'  => $employee->supervisor_id 

		];
	}

	public function viewTasks($id)
	{
		//dd($id);
		$view=TaskManager::with(['employee.designation','employee.supervisor','supervisor'])->find($id);
		//dd($view->toArray());

		return view('view_tasks',['viewList'=>$view]);
	}


	public function editTaskManager($id)
	{
		//dd($id);
		$supervisor=Employees::join('users', 'users.id', '=', 'employees.user_id')
			->select('users.id')
			->selectRaw('CONCAT(employees.first_name," ", employees.last_name) as name')
			->where('users.status', '1')
			->get();

		
		$designation=Designation::get();
		//dd($designation->toArray());
		$edit=TaskManager::with('supervisor')->find($id);
		//dd($edit->toArray());
		return view('edit_task_manager',['edit'=>$edit,'employees'=>$supervisor,'designations'=>$designation]);
	}

	public function editInfo(Request $request)
	{
		$createTask= TaskManager::find($request->id);

		$createTask->employee_id=$request->employee_id;
		$createTask->month=$request->month;
		$createTask->task_name=$request->task_name;
		$createTask->task_description=$request->task_description;
		$createTask->complexity=$request->complexity;
		$createTask->type=$request->type;
		$createTask->task_supervisor=$request->task_supervisor;
		$createTask->allocation_time=$request->allocation_time;
		//$createTask->allocation_date=$request->allocation_date;
		$createTask->allocated_time=$request->allocated_time;
		$createTask->save();

		return redirect()->route('task_manager_Lists_route');
	}




}