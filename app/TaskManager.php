<?php

namespace App;
use App\Employees;

use Illuminate\Database\Eloquent\Model;

class TaskManager extends Model
{
	protected $guarded = [];

  
    public function employee()
    {
    	return $this->belongsTo(Employees::class,'employee_id');
    }

     public function supervisor()
    {
    	return $this->belongsTo(Employees::class , 'task_supervisor');
    }

}
