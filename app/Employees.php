<?php

namespace App;
use App\TaskManager;
use App\Users;
use App\Designation;

use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
	public function designation()
	{
		return $this->belongsTo(Designation::class,'designation_id');
	}

	public function user()
	{
		return $this->belongsTo(Users::class,'user_id');
	}
	public function supervisor()
	{
		return $this->belongsTo(Employees::class,'supervisor_id');
	}
	
}
