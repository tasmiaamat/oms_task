<?php

namespace App;
use App\Employees;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
   protected $table ='users';

   public function employee()
   {
   	return $this->hasOne(Employees::class,'user_id');
   }
}
