<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard','TaskManagement@dashboard')->name('dashboard_route');

Route::get('/task_manager','TaskManagement@taskManager')->name('task_manager_route');

Route::post('/save_task_manager','TaskManagement@saveTaskManager')->name('save_task_manager_route');

Route::get('/task_manager_Lists','TaskManagement@taskManagerLists')->name('task_manager_Lists_route');

Route::post('/get_employee_info','TaskManagement@getEmployeeInfo')->name('get_employee_info_route');

Route::get('/dashboard','TaskManagement@dashboard')->name('dashboard_route');

Route::get('/view_tasks/{id}','TaskManagement@viewTasks')->name('view_tasks_route');

Route::get('/edit_task_manager/{id}','TaskManagement@editTaskManager')->name('edit_task_manager_route');

Route::post('/edit_info','TaskManagement@editInfo')->name('edit_info_route');




