<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskManagersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_managers', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('employee_id');
            $table->string('task_name');
            $table->text('task_description');
            $table->enum('complexity',['0','1']);
            $table->enum('type',['0','1']);
            $table->Integer('task_supervisor');
            $table->date('allocation_date');
            $table->datetime('allocation_time');
            $table->float('allocated_time', 4, 2);
            $table->enum('task_status',['0','1']);
            $table->Integer('created_by')->nullabe();
            $table->timestamps('created_at')->nullabe();
            $table->Integer('updated_by')->nullabe();
            $table->timestamps('updated_at')->nullabe();
            $table->timestamps('deleted_at')->nullabe();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_managers');
    }
}
