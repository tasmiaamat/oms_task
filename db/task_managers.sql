-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 06, 2018 at 01:16 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `safara_oms_practice`
--

-- --------------------------------------------------------

--
-- Table structure for table `task_managers`
--

CREATE TABLE `task_managers` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(10) UNSIGNED NOT NULL,
  `month` varchar(38) NOT NULL,
  `task_name` varchar(155) NOT NULL,
  `task_description` text NOT NULL,
  `complexity` enum('0','1') NOT NULL COMMENT '0=easy,1=hard',
  `type` enum('0','1') NOT NULL COMMENT '0=team,1=individual',
  `task_supervisor` int(10) NOT NULL,
  `allocation_time` datetime NOT NULL,
  `allocated_time` varchar(100) NOT NULL,
  `task_status` enum('0','1') NOT NULL COMMENT '0=on going,1=completed',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `task_managers`
--

INSERT INTO `task_managers` (`id`, `employee_id`, `month`, `task_name`, `task_description`, `complexity`, `type`, `task_supervisor`, `allocation_time`, `allocated_time`, `task_status`, `created_by`, `created_at`, `updated_by`, `updated_at`, `deleted_at`) VALUES
(5, 38, '2018-12', 'Task Management', 'Create a view page,make database', '0', '0', 2, '2019-12-02 04:50:49', '8 hours', '0', NULL, '2018-12-06 11:28:01', NULL, '2018-12-06 04:51:31', NULL),
(6, 11, '2018-12', 'jkjk', 'opopo', '1', '1', 2, '2018-12-06 16:55:29', '8 hours', '0', NULL, '2018-12-06 04:55:44', NULL, '2018-12-06 04:55:44', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `task_managers`
--
ALTER TABLE `task_managers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_id` (`employee_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `task_managers`
--
ALTER TABLE `task_managers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `task_managers`
--
ALTER TABLE `task_managers`
  ADD CONSTRAINT `task_managers_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
